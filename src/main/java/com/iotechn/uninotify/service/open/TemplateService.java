package com.iotechn.uninotify.service.open;

import com.iotechn.uninotify.annotation.HttpMethod;
import com.iotechn.uninotify.annotation.HttpOpenApi;
import com.iotechn.uninotify.annotation.HttpParam;
import com.iotechn.uninotify.annotation.HttpParamType;
import com.iotechn.uninotify.annotation.param.NotNull;
import com.iotechn.uninotify.exception.ServiceException;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/12/26
 * Time: 23:22
 */
@HttpOpenApi(group = "template", description = "模板服务")
public interface TemplateService {

    @HttpMethod(description = "获取所有可用模板")
    public List getValidTemplates() throws ServiceException;

    @HttpMethod(description = "自定义推送", detail = "data提交格式为微信文档上要求的格式。 其中 first 和 remark 是特殊的，分别为头尾。例子：{\"first\":{\"value\":\"恭喜你购买成功！\",\"color\":\"#173177\"},\"keyword1\":{\"value\":\"巧克力\",\"color\":\"#173177\"},\"keyword2\":{\"value\":\"39.8元\",\"color\":\"#173177\"},\"keyword3\":{\"value\":\"2014年9月22日\",\"color\":\"#173177\"},\"remark\":{\"value\":\"欢迎再次购买！\",\"color\":\"#173177\"}}")
    public String customNotify(
            @NotNull @HttpParam(name = "userId", type = HttpParamType.COMMON, description = "开发者方用户ID") String userId,
            @NotNull @HttpParam(name = "templateId", type = HttpParamType.COMMON, description = "微信模板消息Id") String templateId,
            @NotNull @HttpParam(name = "data", type = HttpParamType.COMMON, description = "携带数据的JSON字符串") String data) throws ServiceException;

    @HttpMethod(description = "新订单预设推送, 仅需要相关字段即可")
    public String newOrderNotify(
            @NotNull @HttpParam(name = "userId", type = HttpParamType.COMMON, description = "开发者方用户ID") String userId,
            @NotNull @HttpParam(name = "orderNo", type = HttpParamType.COMMON, description = "订单No") String orderNo,
            @HttpParam(name = "actualPrice", type = HttpParamType.COMMON, description = "支付金额") String actualPrice,
            @HttpParam(name = "payChannel", type = HttpParamType.COMMON, description = "支付方式") String payChannel,
            @HttpParam(name = "consignee", type = HttpParamType.COMMON, description = "签收人") String consignee,
            @HttpParam(name = "phone", type = HttpParamType.COMMON, description = "手机号") String phone,
            @HttpParam(name = "address", type = HttpParamType.COMMON, description = "签收地址") String address,
            @HttpParam(name = "skuInfo", type = HttpParamType.COMMON, description = "订单商品信息") String skuInfo) throws ServiceException;

    @HttpMethod(description = "订单退款预设通知")
    public String refundOrderNotify(
            @NotNull @HttpParam(name = "userId", type = HttpParamType.COMMON, description = "开发者方用户ID") String userId,
            @HttpParam(name = "refundPrice", type = HttpParamType.COMMON, description = "退款金额") String refundPrice,
            @HttpParam(name = "skuInfo", type = HttpParamType.COMMON, description = "订单商品信息") String skuInfo,
            @NotNull @HttpParam(name = "orderNo", type = HttpParamType.COMMON, description = "订单No") String orderNo) throws ServiceException;


}
