package com.iotechn.uninotify.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 开发者的用户，意为开发者将需要被通知的用户存在uninotify的数据库中。通过开发者方用户Id来进行用户定位，发送通知
 * User: rize
 * Date: 2019/12/26
 * Time: 15:32
 */
@Data
@TableName("uninotify_developer_user")
public class DeveloperUserDO extends SuperDO {

    @TableField("app_id")
    private String appId;

    /**
     * 开发者方用户Id
     */
    @TableField("user_id")
    private String userId;

    @TableField("open_id")
    private String openId;

}
